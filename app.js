angular.module("Webmail", [])
.controller("WebmailCtrl", function($scope) {

  $scope.dossiers = [
    { value: "RECEPTION", label: "Boite de réception", emails: [
      { id: 1, from: "Ingrid", to: "Camille", subject: "Salut", date: "24/10/2016", content: "Iam virtutem ex consuetudine vitae sermonisque nostri interpretemur nec eam, ut quidam docti, verborum magnificentia metiamur virosque bonos eos, qui habentur, numeremus, Paulos, Catones, Galos, Scipiones, Philos; his communis vita contenta est; eos autem omittamus, qui omnino nusquam reperiuntur."},
      { id: 2, from: "Jérémy", to: "Aurélie", subject: "Bonjour", date: "24/10/2016", content: "Iam virtutem ex consuetudine vitae sermonisque nostri interpretemur nec eam, ut quidam docti, verborum magnificentia metiamur virosque bonos eos, qui habentur, numeremus, Paulos, Catones, Galos, Scipiones, Philos; his communis vita contenta est; eos autem omittamus, qui omnino nusquam reperiuntur."},
      { id: 3, from: "Philippe", to: "David", subject: "Hey", date: "24/10/2016", content: "Iam virtutem ex consuetudine vitae sermonisque nostri interpretemur nec eam, ut quidam docti, verborum magnificentia metiamur virosque bonos eos, qui habentur, numeremus, Paulos, Catones, Galos, Scipiones, Philos; his communis vita contenta est; eos autem omittamus, qui omnino nusquam reperiuntur."},
      { id: 4, from: "Loïs", to: "Victor", subject: "Salut", date: "24/10/2016", content: "Iam virtutem ex consuetudine vitae sermonisque nostri interpretemur nec eam, ut quidam docti, verborum magnificentia metiamur virosque bonos eos, qui habentur, numeremus, Paulos, Catones, Galos, Scipiones, Philos; his communis vita contenta est; eos autem omittamus, qui omnino nusquam reperiuntur."}
    ] },
    { value: "ARCHIVES", label: "Archives", emails: [
      { id: 5, from: "Pierre", to: "Younès", subject: "Salut", date: "24/10/2016", content: "Iam virtutem ex consuetudine vitae sermonisque nostri interpretemur nec eam, ut quidam docti, verborum magnificentia metiamur virosque bonos eos, qui habentur, numeremus, Paulos, Catones, Galos, Scipiones, Philos; his communis vita contenta est; eos autem omittamus, qui omnino nusquam reperiuntur."},
      { id: 6, from: "Rémy", to: "Brahim", subject: "Bonjour", date: "24/10/2016", content: "Iam virtutem ex consuetudine vitae sermonisque nostri interpretemur nec eam, ut quidam docti, verborum magnificentia metiamur virosque bonos eos, qui habentur, numeremus, Paulos, Catones, Galos, Scipiones, Philos; his communis vita contenta est; eos autem omittamus, qui omnino nusquam reperiuntur."},
      { id: 7, from: "J-P", to: "Maxence", subject: "Hey", date: "24/10/2016", content: "Iam virtutem ex consuetudine vitae sermonisque nostri interpretemur nec eam, ut quidam docti, verborum magnificentia metiamur virosque bonos eos, qui habentur, numeremus, Paulos, Catones, Galos, Scipiones, Philos; his communis vita contenta est; eos autem omittamus, qui omnino nusquam reperiuntur."}
    ] },
    { value: "ENVOYES", label: "Envoyés", emails: [
      { id: 8, from: "Sélim", to: "Kilian", subject: "Salut", date: "24/10/2016", content: "Iam virtutem ex consuetudine vitae sermonisque nostri interpretemur nec eam, ut quidam docti, verborum magnificentia metiamur virosque bonos eos, qui habentur, numeremus, Paulos, Catones, Galos, Scipiones, Philos; his communis vita contenta est; eos autem omittamus, qui omnino nusquam reperiuntur."},
      { id: 9, from: "Léo", to: "Amélie", subject: "Bonjour", date: "24/10/2016", content: "Iam virtutem ex consuetudine vitae sermonisque nostri interpretemur nec eam, ut quidam docti, verborum magnificentia metiamur virosque bonos eos, qui habentur, numeremus, Paulos, Catones, Galos, Scipiones, Philos; his communis vita contenta est; eos autem omittamus, qui omnino nusquam reperiuntur."}
    ] },
    { value: "SPAM", label: "Courrier indésirable", emails: [
      { id: 10, from: "François", to: "Victor", subject: "Hey", date: "24/10/2016", content: "Iam virtutem ex consuetudine vitae sermonisque nostri interpretemur nec eam, ut quidam docti, verborum magnificentia metiamur virosque bonos eos, qui habentur, numeremus, Paulos, Catones, Galos, Scipiones, Philos; his communis vita contenta est; eos autem omittamus, qui omnino nusquam reperiuntur."},
      { id: 11, from: "Christopher", to: "Marielle", subject: "Salut", date: "24/10/2016", content: "Iam virtutem ex consuetudine vitae sermonisque nostri interpretemur nec eam, ut quidam docti, verborum magnificentia metiamur virosque bonos eos, qui habentur, numeremus, Paulos, Catones, Galos, Scipiones, Philos; his communis vita contenta est; eos autem omittamus, qui omnino nusquam reperiuntur."}
    ] }
  ];

  $scope.dossierCourant = null;
  $scope.emailSelectionne = null;

  $scope.selectionDossier = function(dossier) {
    $scope.dossierCourant = dossier;
  }

  $scope.selectionEmail = function(email) {
    $scope.emailSelectionne = email;
  }

});
